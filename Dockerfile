FROM node:12.22-buster
WORKDIR /home/rodkiller/DockerReact
COPY package.json /home/rodkiller/DockerReact
RUN npm install
COPY . /home/rodkiller/DockerReact
EXPOSE 3000
CMD ["npm","run","start"]

